<?php

use Illuminate\Database\Seeder;

class BiografisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Biografi::truncate();
    
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Biografi::create([
                'title' => $faker->sentence,
                'description' => $faker->paragraph,
                'image' => $faker->sentence,
            ]);
        }
        //
    }
}
