<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $fillable = ['title', 'description', 'media_type', 'file_url'];
    public $timestamps = false;
}
