<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    protected $fillable = ['title', 'description', 'image_name', 'image_url'];
    public $timestamps = false;
}
