<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;

class MediaController extends Controller
{
    public function index() {
        return Media::all();
    }

    public function show($id){
        return Media::find($id);
    }

    public function create(Request $request){
        $media = Media::create($request->all());
        return response()->json($media, 201);
    }

    public function update(Request $request, $id){
        $media = Media::findOrFail($id);
        $media->update($request->all());
        return response()->json($media, 200);
    }

    public function delete(Request $request, $id){
        $media = Media::findOrFail($id);
        $media->delete();

        return response()->json(null,204);
    }
}
