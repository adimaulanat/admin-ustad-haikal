<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class BeritaController extends Controller
{
    public function index() {
        return Berita::all();
    }

    public function show($id){
        return Berita::find($id);
    }

    public function create(Request $request){
        $berita = Berita::create($request->all());
        return response()->json($berita, 201);
    }

    public function update(Request $request, $id){
        $berita = Berita::findOrFail($id);
        $berita->update($request->all());
        return response()->json($berita, 200);
    }

    public function delete(Request $request, $id){
        $berita = Berita::findOrFail($id);
        $berita->delete();

        return response()->json(null,204);
    }
}
