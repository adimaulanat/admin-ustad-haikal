<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biografi;
use App\Http\Resources\Biografi as BiografiResource;

class BiografiController extends Controller
{
    public function index() {
        return Biografi::all();
    }

    public function show($id){
        return Biografi::find($id);
    }

    public function create(Request $request){
        $biografi = Biografi::create($request->all());
        return response()->json($biografi, 201);
    }

    public function update(Request $request, $id){
        $biografi = Biografi::findOrFail($id);
        $biografi->update($request->all());
        return response()->json($biografi, 200);
    }

    public function delete(Request $request, $id){
        $biografi = Biografi::findOrFail($id);
        $biografi->delete();

        return response()->json(null,204);
    }
    //
}
