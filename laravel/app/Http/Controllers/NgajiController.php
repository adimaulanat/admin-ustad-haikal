<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ngaji;

class NgajiController extends Controller
{
    public function index() {
        return Ngaji::all();
    }

    public function show($id){
        return Ngaji::find($id);
    }

    public function create(Request $request){
        $ngaji = Ngaji::create($request->all());
        return response()->json($ngaji, 201);
    }

    public function update(Request $request, $id){
        $ngaji = Ngaji::findOrFail($id);
        $ngaji->update($request->all());
        return response()->json($ngaji, 200);
    }

    public function delete(Request $request, $id){
        $ngaji = Ngaji::findOrFail($id);
        $ngaji->delete();

        return response()->json(null,204);
    }
}
