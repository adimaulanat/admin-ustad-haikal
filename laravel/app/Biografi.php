<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biografi extends Model
{
    protected $table = 'biografi';
    protected $fillable = ['title', 'description', 'image_name', 'image_url'];
    public $timestamps = false;
}
