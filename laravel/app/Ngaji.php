<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ngaji extends Model
{
    protected $table = 'ngaji';
    protected $fillable = ['title', 'description', 'speakers', 'event_date', 'event_time', 'event_place', 'image_url', 'image_name'];
    public $timestamps = false;
}
