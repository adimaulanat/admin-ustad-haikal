<?php

use Illuminate\Http\Request;
use App\Biografi;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'cors'], function() {
    Route::post('login', 'API\UserController@login');
    Route::post('register', 'API\UserController@register');
    Route::get('details/{id}', 'API\UserController@show');
    
    Route::get('/artikel', 'ArtikelController@index');
    Route::get('/artikel/{id}', 'ArtikelController@show');
    Route::post('/artikel', 'ArtikelController@create');
    Route::put('/artikel/{id}', 'ArtikelController@update');
    Route::delete('/artikel/{id}', 'ArtikelController@delete');

    Route::get('/biografi', 'BiografiController@index');
    Route::get('/biografi/{id}', 'BiografiController@show');
    Route::post('/biografi', 'BiografiController@create');
    Route::put('/biografi/{id}', 'BiografiController@update');
    Route::delete('/biografi/{id}', 'BiografiController@delete');

    Route::get('/berita', 'BeritaController@index');
    Route::get('/berita/{id}', 'BeritaController@show');
    Route::post('/berita', 'BeritaController@create');
    Route::put('/berita/{id}', 'BeritaController@update');
    Route::delete('/berita/{id}', 'BeritaController@delete');

    Route::get('/media', 'MediaController@index');
    Route::get('/media/{id}', 'MediaController@show');
    Route::post('/media', 'MediaController@create');
    Route::put('/media/{id}', 'MediaController@update');
    Route::delete('/media/{id}', 'MediaController@delete');

    Route::get('/ngaji', 'NgajiController@index');
    Route::get('/ngaji/{id}', 'NgajiController@show');
    Route::post('/ngaji', 'NgajiController@create');
    Route::put('/ngaji/{id}', 'NgajiController@update');
    Route::delete('/ngaji/{id}', 'NgajiController@delete');
});

