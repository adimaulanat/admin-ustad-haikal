// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDnodrOQuuHf7ttwLFR58dKKW2y3Atn4pk",
    authDomain: "cms-ustad-haikal.firebaseapp.com",
    databaseURL: "https://cms-ustad-haikal.firebaseio.com",
    projectId: "cms-ustad-haikal",
    storageBucket: "cms-ustad-haikal.appspot.com",
    messagingSenderId: "423851779822"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
