import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable, Output, EventEmitter } from '@angular/core';
import { map } from "rxjs/operators";

import { URLServices, api } from './url-services';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase';

// const endpoint = 'http://127.0.0.1:8000/api';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
};
@Injectable()
export class AuthServices {
    private basePath = '/login';
    private loggedIn = new BehaviorSubject<boolean>(false);
    @Output() getLoggedInStatus: EventEmitter<any> = new EventEmitter()

    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }

    constructor(public http: HttpClient, public urlServices: URLServices, private db: AngularFireDatabase) {
    }
    /**
     * Return observeable of master data request
     * @param _encrypted 
     * @param Authorization 
     */

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    public makeLoginTrue(){
        this.getLoggedInStatus.emit(true)
        this.loggedIn.next(true)
    }

    public login(body): Observable<any> {
        // console.log(body)
        return this.http.post(api + this.basePath, body, httpOptions).pipe(
            map(this.extractData))
    }

    logout() {
        this.loggedIn.next(false);
        this.getLoggedInStatus.emit(false)
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    public register(body): Observable<any> {
        console.log(body)
        return this.http.post(api + '/register', body, httpOptions).pipe(
            map(this.extractData))
    }

    private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;

        if (error instanceof HttpErrorResponse) {
            const err = error.message || JSON.stringify(error);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            errObj = error.message;
        } else {
            errMsg = error.message ? error.message : error.toString();
            const body = error.message || '';
            errObj = body;
        }

        return Observable.throw(errObj);
    }



}