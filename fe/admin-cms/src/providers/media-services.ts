import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { map, catchError } from "rxjs/operators";

import { URLServices, api } from './url-services';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase';

const endpoint = 'http://127.0.0.1:8000/api';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
};
@Injectable()
export class MediaServices {
    private basePath = '/media';

    constructor(public http: HttpClient, public urlServices: URLServices, private db: AngularFireDatabase) {
    }
    /**
     * Return observeable of master data request
     * @param _encrypted 
     * @param Authorization 
     */

    pushFileToStorage(model, progress: { percentage: number }, isUpdate?:boolean, idUpdate?) {
        let storageRef = firebase.storage().ref();
        let uploadTask = storageRef.child(`${this.basePath}/${model.file_url.file.name}`).put(model.file_url.file)
        uploadTask.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                const snap = snapshot as firebase.storage.UploadTaskSnapshot
                progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
            },
            (error) => {
                console.log(error)
            },
            () => {

                uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                        model.file_url = downloadURL
                    
                    
                    if(isUpdate){
                        this.updateMedia(idUpdate, model).subscribe(() => {
                            console.log('Success update media')
                        }, (err) =>{
                            console.log(err)
                        });
                    } else{
                        this.createMedia(model).subscribe(() => {
                           console.log('Success upload media')
                       }, (err) =>{
                           console.log(err)
                       });
                    }
                });
            }
        )

    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    public createMedia(body): Observable<any> {
        console.log(body)
        return this.http.post(api + this.basePath , body, httpOptions).pipe(
            map(this.extractData))
    }

    public getMasterMedia() {
        return this.http.get(api + this.basePath).pipe(
            map(this.extractData))
    }

    public getMediaById(id: any): Observable<any> {
        return this.http.get(api + this.basePath + '/' + id).pipe(
            map(this.extractData))
    }

    public updateMedia(id: any, body): Observable<any> {
        return this.http.put(api + this.basePath + '/' + id, body, httpOptions).pipe(
            map(this.extractData))
    }

    public deleteMedia(id: any): Observable<any> {
        return this.http.delete(api + this.basePath + '/' + id).pipe(
            map(this.extractData))
    }

    // public deleteBeritaById($key) {
        // this.beritaList.remove($key)
    // }



    private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;

        if (error instanceof HttpErrorResponse) {
            const err = error.message || JSON.stringify(error);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            errObj = error.message;
        } else {
            errMsg = error.message ? error.message : error.toString();
            const body = error.message || '';
            errObj = body;
        }

        return Observable.throw(errObj);
    }



}