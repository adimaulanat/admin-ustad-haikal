import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { map, catchError } from "rxjs/operators";

import { URLServices, api } from './url-services';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase';

const endpoint = 'http://127.0.0.1:8000/api';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
};
@Injectable()
export class ArtikelServices {
    private basePath = '/artikel';

    constructor(public http: HttpClient, public urlServices: URLServices, private db: AngularFireDatabase) {
    }
    /**
     * Return observeable of master data request
     * @param _encrypted 
     * @param Authorization 
     */

    pushFileToStorage(model, progress: { percentage: number }, isUpdate?:boolean, idUpdate?) {
        let storageRef = firebase.storage().ref();
        let uploadTask = storageRef.child(`${this.basePath}/${model.image_url.file.name}`).put(model.image_url.file)
        uploadTask.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                const snap = snapshot as firebase.storage.UploadTaskSnapshot
                progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100)
            },
            (error) => {
                console.log(error)
            },
            () => {

                uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                    if (model.image_url.file != undefined){
                        model.image_name = model.image_url.file.name
                        model.image_url = downloadURL
                    }
                    
                    if(isUpdate){
                        this.updateArtikel(idUpdate, model).subscribe(() => {
                            console.log('Success update artikel')
                        }, (err) =>{
                            console.log(err)
                        });
                    } else{
                        this.createArtikel(model).subscribe(() => {
                           console.log('Success upload artikel')
                       }, (err) =>{
                           console.log(err)
                       });
                    }
                });
            }
        )

    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    public createArtikel(body): Observable<any> {
        console.log(body)
        return this.http.post(api + this.basePath , body).pipe(
            map(this.extractData))
    }

    public getMasterArtikel() {
        return this.http.get(api + this.basePath).pipe(
            map(this.extractData))
    }

    public getArtikelById(id: any): Observable<any> {
        return this.http.get(api + this.basePath + '/' + id).pipe(
            map(this.extractData))
    }

    public updateArtikel(id: any, body): Observable<any> {
        return this.http.put(api + this.basePath + '/' + id, body).pipe(
            map(this.extractData))
    }

    public deleteArtikel(id: any): Observable<any> {
        return this.http.delete(api + this.basePath + '/' + id).pipe(
            map(this.extractData))
    }

    // public deleteBeritaById($key) {
        // this.beritaList.remove($key)
    // }



    private handleError(error: HttpErrorResponse | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        let errObj: any;

        if (error instanceof HttpErrorResponse) {
            const err = error.message || JSON.stringify(error);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            errObj = error.message;
        } else {
            errMsg = error.message ? error.message : error.toString();
            const body = error.message || '';
            errObj = body;
        }

        return Observable.throw(errObj);
    }



}