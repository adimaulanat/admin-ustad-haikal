import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AuthGuard } from './_guard/auth.guard';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminModule } from './admin/admin.module';
import { BeritaServices } from 'src/providers/berita-services';
import { HttpClientModule } from '@angular/common/http';
import { URLServices } from 'src/providers/url-services';
import { PengajianServices } from 'src/providers/pengajian-services';
import { BiografiServices } from 'src/providers/biografi-services';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import * as firebase from 'firebase';
import { ArtikelServices } from 'src/providers/artikel-services';
import { MediaServices } from 'src/providers/media-services';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { FormLoginComponent } from './login/form-login/form-login.component';
import { AuthServices } from 'src/providers/auth-services';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormRegisterComponent } from './register/form-register/form-register.component'

const appRoutes: Routes = [
  { path: 'admin', canActivate: [AuthGuard], loadChildren: './admin/admin.module#AdminModule'},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: FormLoginComponent },
  { path: 'register', component: FormRegisterComponent }
]

firebase.initializeApp(environment.firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    FormLoginComponent,
    HeaderComponent,
    FooterComponent,
    FormRegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AdminModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    Ng4LoadingSpinnerModule.forRoot(),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  exports: [
    RouterModule
  ],
  providers: [
    ArtikelServices,
    BeritaServices,
    BiografiServices,
    MediaServices,
    PengajianServices,
    URLServices,
    AuthServices,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
