import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FileUpload } from 'src/shared/models/file-upload';
import { Router } from '@angular/router';
import { FileValidator } from 'src/shared/validators/input-file.validators';

@Component({
  selector: 'master-artikel-form',
  templateUrl: './master-artikel-form.component.html',
  styleUrls: ['./master-artikel-form.component.scss']
})
export class MasterArtikelFormComponent implements OnInit {

  @Output('init') onInit = new EventEmitter()
  @Output('save') onSave = new EventEmitter()

  fg:FormGroup
  errorMsg
  submitting
  selectedFile: File
  currentFileUpload: FileUpload

  constructor(
    private fb:FormBuilder, private router:Router
  ) { 
    this.fg = this.fb.group({
      title: [null, [Validators.required]],
      description: [null, [Validators.required]],
      image_url: new FormControl("", [FileValidator.validate]),
      image_name: null,
      created_date: null
    })
  }

  ngOnInit() {
    this.onInit.emit(this.fg)
  }

  save() {
    this.onSave.emit(this.fg.value)
  }

  cancel() {
    this.router.navigate(['/admin', 'berita'])
  }

  onFileChanged(event){

    if(event.target.files && event.target.files.length) {
      this.currentFileUpload = new FileUpload(<File>event.target.files[0]);
        this.fg.patchValue({
          image_url: this.currentFileUpload,
          image_name: this.currentFileUpload.file.name
       });
    }
    console.log(this.fg)
  }

}
