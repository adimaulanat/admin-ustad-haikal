import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterArtikelFormComponent } from './master-artikel-form.component';
import { ArtikelServices } from 'src/providers/artikel-services';
import { formatDate } from '@angular/common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-artikel-create',
  template: `
    <master-artikel-form (save)="save($event)"></master-artikel-form>
  `,
  styles: []
})
export class MasterArtikelCreateComponent implements OnInit {

  @ViewChild(MasterArtikelFormComponent)
  formComponent: MasterArtikelFormComponent
  progress: { percentage: number } = { percentage: 0 };

  constructor(
    private router:Router, private artikelServices: ArtikelServices, private spinnerService: Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {

  }

  save(model) {
    model.created_date = formatDate(new Date(), 'yyyy/MM/dd', 'en');
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined

    this.spinnerService.show()
    this.artikelServices.pushFileToStorage(model, this.progress);
    setTimeout(() => {
      this.router.navigate(['/admin', 'artikel'])
      this.formComponent.submitting = false
      this.spinnerService.hide()
    },6500)
    
  }

}
