import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterArtikelFormComponent } from './master-artikel-form.component';
import { ArtikelServices } from 'src/providers/artikel-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'master-artikel-edit',
    template: `
    <master-artikel-form (init)="init($event)" (save)="save($event)"></master-artikel-form>
  `,
    styles: []
})
export class MasterArtikelEditComponent implements OnInit {

    @ViewChild(MasterArtikelFormComponent)
    formComponent: MasterArtikelFormComponent
    artikel
    progress: { percentage: number } = { percentage: 0 };



    constructor(
        private router: Router, private route: ActivatedRoute, private artikelServices: ArtikelServices, private spinnerService: Ng4LoadingSpinnerService
    ) {

    }

    ngOnInit() {
    }

    init(fg: FormGroup) {
        this.spinnerService.show()
        this.route.params.subscribe(params => {
            let id = params['id']
            this.artikelServices.getArtikelById(id).subscribe(data => {
                this.artikel = data
                fg.patchValue(this.artikel);
                this.spinnerService.hide()
            })
        })
    }

    save(model) {
        console.log(model.image_name, this.artikel.image_name)
        this.formComponent.submitting = true
        this.formComponent.errorMsg = undefined
        if (model.image_name == this.artikel.image_name) {
            this.spinnerService.show()
            this.artikelServices.updateArtikel(this.artikel.id, model).subscribe(() => {
                this.router.navigate(['/admin', 'artikel'])
                this.formComponent.submitting = false
                this.spinnerService.hide()
            }, err => {
                this.formComponent.errorMsg = err.message
                this.formComponent.submitting = false
                this.spinnerService.hide()
            })
        } else {
            this.spinnerService.show()
            this.artikelServices.pushFileToStorage(model, this.progress, true, this.artikel.id)
            setTimeout(() => {
                this.router.navigate(['/admin', 'artikel'])
                this.formComponent.submitting = false
                this.spinnerService.hide()
            }, 6500)
        }

    }

}
