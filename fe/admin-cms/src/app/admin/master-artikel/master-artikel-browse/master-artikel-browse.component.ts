import { Component, OnInit } from '@angular/core';
import { promptDialog } from 'src/shared/promp.modal';
import { ArtikelServices } from 'src/providers/artikel-services';

@Component({
  selector: 'app-master-artikel-browse',
  templateUrl: './master-artikel-browse.component.html',
  styleUrls: ['./master-artikel-browse.component.scss']
})
export class MasterArtikelBrowseComponent implements OnInit {

  errorMsg
  masterArtikel: any = [];

  constructor(private artikelServices: ArtikelServices) { }

  ngOnInit() {
    this.artikelServices.getMasterArtikel().subscribe((response) => {
      console.log(response);
      this.masterArtikel = response;
    },(err) => {
      console.log(err);
    })
  }

  delete(item) {
    this.errorMsg = undefined

    promptDialog('Delete this record?', 'after deleting, the record will not be recoverable', () => {
      this.artikelServices.deleteArtikel(item.id).subscribe(data => {
        this.masterArtikel = this.masterArtikel.filter(u => u.id !== item.id)
      }, err => {
        this.errorMsg = err.message
      })
    }, () => {})
  }


}
