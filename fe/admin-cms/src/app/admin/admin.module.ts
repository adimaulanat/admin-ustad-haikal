import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { MasterBeritaBrowseComponent } from './master-berita/master-berita-browse/master-berita-browse.component';
import { MasterBeritaCreateComponent } from './master-berita/master-berita-form/master-berita-create.component';
import { MasterBeritaEditComponent } from './master-berita/master-berita-form/master-berita-edit.component';
import { MasterBeritaComponent } from './master-berita/master-berita.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MasterBeritaFormComponent } from './master-berita/master-berita-form/master-berita-form.component';
import { MasterNgajiComponent } from './master-ngaji/master-ngaji.component';
import { MasterBiografiComponent } from './master-biografi/master-biografi.component';
import { MasterBiografiBrowseComponent } from './master-biografi/master-biografi-browse/master-biografi-browse.component';
import { MasterBiografiFormComponent } from './master-biografi/master-biografi-form/master-biografi-form.component';
import { MasterNgajiFormComponent } from './master-ngaji/master-ngaji-form/master-ngaji-form.component';
import { MasterNgajiBrowseComponent } from './master-ngaji/master-ngaji-browse/master-ngaji-browse.component';
import { MasterNgajiCreateComponent } from './master-ngaji/master-ngaji-form/master-ngaji-create.component';
import { MasterNgajiEditComponent } from './master-ngaji/master-ngaji-form/master-ngaji-edit.component';
import { MasterBiografiCreateComponent } from './master-biografi/master-biografi-form/master-biografi-create.component';
import { MasterBiografiEditComponent } from './master-biografi/master-biografi-form/master-biografi-edit.component';
import { FileValueAccessor } from 'src/shared/file-control-value-accessor';
import { FileValidator } from 'src/shared/validators/input-file.validators';
import { MasterArtikelBrowseComponent } from './master-artikel/master-artikel-browse/master-artikel-browse.component';
import { MasterArtikelFormComponent } from './master-artikel/master-artikel-form/master-artikel-form.component';
import { MasterArtikelCreateComponent } from './master-artikel/master-artikel-form/master-artikel-create.component';
import { MasterArtikelEditComponent } from './master-artikel/master-artikel-form/master-artikel-edit.component';
import { MasterArtikelComponent } from './master-artikel/master-artikel.component';
import { MasterMediaComponent } from './master-media/master-media.component';
import { MasterMediaBrowseComponent } from './master-media/master-media-browse/master-media-browse.component';
import { MasterMediaFormComponent } from './master-media/master-media-form/master-media-form.component';
import { MasterMediaCreateComponent } from './master-media/master-media-form/master-media-create.component';
import { MasterMediaEditComponent } from './master-media/master-media-form/master-media-edit.component';
import { AuthGuard } from '../_guard/auth.guard';

const appRoutes: Routes = [
    { path: 'admin', component: AdminComponent, canActivate: [AuthGuard], children: [
        { path: '', redirectTo: 'admin', pathMatch: 'full' },
        { path: 'artikel', component: MasterArtikelComponent, children: [
          { path: '', redirectTo: 'browse', pathMatch: 'full' },
          { path: 'browse', component: MasterArtikelBrowseComponent  },
          { path: 'create', component:  MasterArtikelCreateComponent },
          { path: ':id/edit', component:  MasterArtikelEditComponent }
        ] },
        { path: 'berita', component: MasterBeritaComponent, children: [
            { path: '', redirectTo: 'browse', pathMatch: 'full' },
            { path: 'browse', component: MasterBeritaBrowseComponent  },
            { path: 'create', component:  MasterBeritaCreateComponent },
            { path: ':id/edit', component:  MasterBeritaEditComponent }
          ] },
          { path: 'biografi', component: MasterBiografiComponent, children: [
            { path: '', redirectTo: 'browse', pathMatch: 'full' },
            { path: 'browse', component: MasterBiografiBrowseComponent  },
            { path: 'create', component:  MasterBiografiCreateComponent },
            { path: ':id/edit', component:  MasterBiografiEditComponent }
          ] },
          { path: 'media', component: MasterMediaComponent, children: [
            { path: '', redirectTo: 'browse', pathMatch: 'full' },
            { path: 'browse', component: MasterMediaBrowseComponent  },
            { path: 'create', component:  MasterMediaCreateComponent },
            { path: ':id/edit', component:  MasterMediaEditComponent }
          ] },
          { path: 'ngaji', component: MasterNgajiComponent, children: [
            { path: '', redirectTo: 'browse', pathMatch: 'full' },
            { path: 'browse', component: MasterNgajiBrowseComponent  },
            { path: 'create', component:  MasterNgajiCreateComponent },
            { path: ':id/edit', component:  MasterNgajiEditComponent }
          ] },
    ]}
]


@NgModule({
  declarations: [
      AdminComponent,
      MasterBeritaComponent,
      MasterBeritaFormComponent,
      MasterBeritaBrowseComponent,
      MasterBeritaCreateComponent,
      MasterBeritaEditComponent,
      MasterBiografiComponent,
      MasterBiografiBrowseComponent,
      MasterBiografiFormComponent,
      MasterBiografiCreateComponent,
      MasterBiografiEditComponent,
      MasterNgajiComponent,
      MasterNgajiFormComponent,
      MasterNgajiBrowseComponent,
      MasterNgajiCreateComponent,
      MasterNgajiEditComponent,
      FileValueAccessor,
      FileValidator,
      MasterArtikelComponent,
      MasterArtikelBrowseComponent,
      MasterArtikelFormComponent,
      MasterArtikelCreateComponent,
      MasterArtikelEditComponent,
      MasterMediaComponent,
      MasterMediaBrowseComponent,
      MasterMediaFormComponent,
      MasterMediaEditComponent,
      MasterMediaCreateComponent
  ],
  exports: [
      MasterArtikelComponent,
      MasterBeritaComponent,
      MasterBeritaFormComponent,
      MasterBiografiComponent,
      MasterMediaComponent,
      MasterNgajiComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes)
  ],
  providers: [
  ],
  bootstrap: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
})
export class AdminModule { }
