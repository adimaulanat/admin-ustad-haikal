import { Component, OnInit } from '@angular/core';
import { promptDialog } from 'src/shared/promp.modal';
import { BeritaServices } from 'src/providers/berita-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-master-berita-browse',
  templateUrl: './master-berita-browse.component.html',
  styleUrls: ['./master-berita-browse.component.scss']
})
export class MasterBeritaBrowseComponent implements OnInit {

  errorMsg
  masterBerita: any = [];

  constructor(private beritaServices: BeritaServices, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show()
    this.beritaServices.getMasterBerita().subscribe((response) => {
      console.log(response);
      this.masterBerita = response;
      this.spinnerService.hide()
    },(err) => {
      this.spinnerService.hide()
      console.log(err);
    })
  }

  delete(item) {
    this.errorMsg = undefined

    promptDialog('Delete this record?', 'after deleting, the record will not be recoverable', () => {
      this.spinnerService.show()
      this.beritaServices.deleteBerita(item.id).subscribe(data => {
        this.masterBerita = this.masterBerita.filter(u => u.id !== item.id)
        this.spinnerService.hide()
      }, err => {
        this.errorMsg = err.message
        this.spinnerService.hide()
      })
    }, () => {})
  }

}
