import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterBeritaFormComponent } from './master-berita-form.component';
import { BeritaServices } from 'src/providers/berita-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-berita-edit',
  template: `
    <master-berita-form (init)="init($event)" (save)="save($event)"></master-berita-form>
  `,
  styles: []
})
export class MasterBeritaEditComponent implements OnInit {

  @ViewChild(MasterBeritaFormComponent)
  formComponent: MasterBeritaFormComponent
  berita
  progress: { percentage: number } = { percentage: 0 };



  constructor(
    private router: Router, private route: ActivatedRoute, private beritaServices: BeritaServices, private spinnerService: Ng4LoadingSpinnerService
  ) {

  }

  ngOnInit() {
  }

  init(fg: FormGroup) {
    this.spinnerService.show()
    this.route.params.subscribe(params => {
      let id = params['id']
      console.log(id)
      this.beritaServices.getBeritaById(id).subscribe(data => {
        this.berita = data
        console.log(this.berita)
        fg.patchValue(this.berita);
        this.spinnerService.hide()
      })
    })
  }

  save(model) {
    console.log(model)
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined

    if (model.image_name == this.berita.image_name) {
      this.spinnerService.show()
      this.beritaServices.updateBerita(this.berita.id, model).subscribe(() => {
        this.router.navigate(['/admin', 'berita'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, err => {
        this.formComponent.errorMsg = err.message
        this.formComponent.submitting = false
        this.spinnerService.hide()
      })
    } else {
      this.spinnerService.show()
      this.beritaServices.pushFileToStorage(model, this.progress, true, this.berita.id)
      setTimeout(() => {
        this.router.navigate(['/admin', 'berita'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, 6500)
    }
  }

}
