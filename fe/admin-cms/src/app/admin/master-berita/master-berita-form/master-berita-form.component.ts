import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FileValidator } from 'src/shared/validators/input-file.validators';
import { FileUpload } from 'src/shared/models/file-upload';

@Component({
  selector: 'master-berita-form',
  templateUrl: './master-berita-form.component.html',
  styleUrls: ['./master-berita-form.component.scss']
})
export class MasterBeritaFormComponent implements OnInit {

  @Output('init') onInit = new EventEmitter()
  @Output('save') onSave = new EventEmitter()

  fg:FormGroup
  errorMsg
  submitting
  currentFileUpload: FileUpload

  constructor(
    private fb:FormBuilder, private router:Router
  ) { 
    this.fg = this.fb.group({
      title: [null, [Validators.required]],
      description: [null, [Validators.required]],
      image_url: new FormControl("", [FileValidator.validate]),
      image_name: null
    })
  }

  ngOnInit() {
    this.onInit.emit(this.fg)
  }

  save() {
    this.onSave.emit(this.fg.value)
  }

  cancel() {
    this.router.navigate(['/admin', 'berita'])
  }

  onFileChanged(event){

    if(event.target.files && event.target.files.length) {
      this.currentFileUpload = new FileUpload(<File>event.target.files[0]);
        this.fg.patchValue({
          image_url: this.currentFileUpload,
          image_name: this.currentFileUpload.file.name
       });
    }
    console.log(this.fg)
    // this.selectedFile = event.target.files[0];
    // console.log(this.selectedFile)
    // this.fg.get('image_url').setValue(this.selectedFile)
    // if(event.target.files.length > 0){
    // }
  }

}
