import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterBeritaFormComponent } from './master-berita-form.component';
import { BeritaServices } from 'src/providers/berita-services';
import { FileUpload } from 'src/shared/models/file-upload';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-berita-create',
  template: `
    <master-berita-form (save)="save($event)"></master-berita-form>
  `,
  styles: []
})
export class MasterBeritaCreateComponent implements OnInit {

  @ViewChild(MasterBeritaFormComponent)
  formComponent: MasterBeritaFormComponent
  progress: { percentage: number } = { percentage: 0 };

  constructor(
    private router:Router, private beritaServices: BeritaServices, private spinnerService: Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {

  }

  save(model) {
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined
    this.spinnerService.show()
    this.beritaServices.pushFileToStorage(model, this.progress);
    setTimeout(() => {
      this.router.navigate(['/admin', 'berita'])
      this.formComponent.submitting = false
      this.spinnerService.hide()
    },6500)

    // this.beritaServices.createBerita(formModel).subscribe(() => {
    //   this.router.navigate(['/admin', 'berita'])
    //   this.formComponent.submitting = false
    // }, (err) => {
    //   this.formComponent.errorMsg = err.message
    //   this.formComponent.submitting = false
    // })
    
  }

}
