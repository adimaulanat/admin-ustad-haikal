import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterNgajiFormComponent } from './master-ngaji-form.component';
import { PengajianServices } from 'src/providers/pengajian-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-ngaji-create',
  template: `
    <master-ngaji-form (save)="save($event)"></master-ngaji-form>
  `,
  styles: []
})
export class MasterNgajiCreateComponent implements OnInit {

  @ViewChild(MasterNgajiFormComponent)
  formComponent: MasterNgajiFormComponent
  progress: { percentage: number } = { percentage: 0 };

  constructor(
    private router:Router, private pengajianServices: PengajianServices, private spinnerService: Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {

  }

  save(model) {
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined
    this.spinnerService.show()
    this.pengajianServices.pushFileToStorage(model, this.progress);
    setTimeout(() => {
      this.router.navigate(['/admin', 'ngaji'])
      this.formComponent.submitting = false
      this.spinnerService.hide()
    },6500)

    // this.pengajianServices.createPengajian(model).subscribe(() => {
    //   this.router.navigate(['/admin', 'ngaji'])
    //   this.formComponent.submitting = false
    // }, (err) => {
    //   this.formComponent.errorMsg = err.message
    //   this.formComponent.submitting = false
    // })
    
  }

}
