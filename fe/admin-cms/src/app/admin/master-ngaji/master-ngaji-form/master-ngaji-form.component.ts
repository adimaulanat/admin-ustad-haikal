import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FileUpload } from 'src/shared/models/file-upload';

@Component({
  selector: 'master-ngaji-form',
  templateUrl: './master-ngaji-form.component.html',
  styleUrls: ['./master-ngaji-form.component.scss']
})
export class MasterNgajiFormComponent implements OnInit {

  @Output('init') onInit = new EventEmitter()
  @Output('save') onSave = new EventEmitter()

  fg:FormGroup
  errorMsg
  submitting
  currentFileUpload: FileUpload;

  constructor(
    private fb:FormBuilder,
    private router:Router
  ) { 
    this.fg = this.fb.group({
      title: [null, [Validators.required]],
      description: [null, [Validators.required]],
      speakers: [null, [Validators.required]],
      event_date: [null, [Validators.required]],
      event_time: [null, [Validators.required]],
      event_place: [null, [Validators.required]],
      image_url: [null, [Validators.required]],
      image_name: null
    })
  }

  ngOnInit() {
    this.onInit.emit(this.fg)
  }

  save() {
    this.onSave.emit(this.fg.value)
  }

  cancel() {
    this.router.navigate(['/admin', 'ngaji'])
  }

  onFileChanged(event){

    if(event.target.files && event.target.files.length) {
      this.currentFileUpload = new FileUpload(<File>event.target.files[0]);
        this.fg.patchValue({
          image_url: this.currentFileUpload,
          image_name: this.currentFileUpload.file.name
       });
    }
    console.log(this.fg)
    // this.selectedFile = event.target.files[0];
    // console.log(this.selectedFile)
    // this.fg.get('image_url').setValue(this.selectedFile)
    // if(event.target.files.length > 0){
    // }
  }

}
