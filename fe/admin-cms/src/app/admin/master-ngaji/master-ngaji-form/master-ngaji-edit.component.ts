import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterNgajiFormComponent } from './master-ngaji-form.component';
import { PengajianServices } from 'src/providers/pengajian-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-ngaji-edit',
  template: `
    <master-ngaji-form (init)="init($event)" (save)="save($event)"></master-ngaji-form>
  `,
  styles: []
})
export class MasterNgajiEditComponent implements OnInit {

  @ViewChild(MasterNgajiFormComponent)
  formComponent: MasterNgajiFormComponent
  jadwalNgaji
  progress: { percentage: number } = { percentage: 0 };



  constructor(
    private router: Router, private route: ActivatedRoute, private pengajianServices: PengajianServices, private spinnerService: Ng4LoadingSpinnerService
  ) {

  }

  ngOnInit() {
  }

  init(fg: FormGroup) {
    this.spinnerService.show()
    this.route.params.subscribe(params => {
      let id = params['id']
      console.log(id)
      this.pengajianServices.getPengajianById(id).subscribe(data => {
        this.jadwalNgaji = data
        console.log(this.jadwalNgaji)
        fg.patchValue(this.jadwalNgaji);
        this.spinnerService.hide()
      })
    })
  }

  save(model) {
    console.log(model)
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined

    if (model.image_name == this.jadwalNgaji.image_name) {
      this.spinnerService.show()
      this.pengajianServices.updatePengajian(this.jadwalNgaji.id, model).subscribe(() => {
        this.router.navigate(['/admin', 'ngaji'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, err => {
        this.formComponent.errorMsg = err.message
        this.formComponent.submitting = false
        this.spinnerService.hide()
      })
    } else {
      this.spinnerService.show()
      this.pengajianServices.pushFileToStorage(model, this.progress, true, this.jadwalNgaji.id)
      setTimeout(() => {
        this.router.navigate(['/admin', 'ngaji'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, 6500)
    }

    // this.pengajianServices.updatePengajian(this.jadwalNgaji.id, model).subscribe(data => {
    //   console.log(data)
    //   this.router.navigate(['/admin', 'ngaji'])
    //   this.formComponent.submitting = false
    // }, err => {
    //   this.formComponent.errorMsg = err.message
    //   this.formComponent.submitting = false
    // });
  }

}
