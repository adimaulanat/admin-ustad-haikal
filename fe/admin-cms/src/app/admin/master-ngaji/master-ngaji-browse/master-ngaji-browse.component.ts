import { Component, OnInit } from '@angular/core';
import { promptDialog } from 'src/shared/promp.modal';
import { PengajianServices } from 'src/providers/pengajian-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-master-ngaji-browse',
  templateUrl: './master-ngaji-browse.component.html',
  styleUrls: ['./master-ngaji-browse.component.scss']
})
export class MasterNgajiBrowseComponent implements OnInit {

  errorMsg
  masterPengajian: any = [];

  constructor(private pengajianServices: PengajianServices, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show()
    this.pengajianServices.getMasterPengajian().subscribe((response) => {
      console.log(response);
      this. masterPengajian = response;
      this.spinnerService.hide()
    },(err) => {
      console.log(err);
      this.spinnerService.hide()
    })
  }

  delete(item) {
    this.errorMsg = undefined

    promptDialog('Delete this record?', 'after deleting, the record will not be recoverable', () => {
      this.spinnerService.show()
      this.pengajianServices.deletePengajianById(item.id).subscribe(data => {
        this.masterPengajian = this.masterPengajian.filter(u => u.id !== item.id)
        this.spinnerService.hide()
      }, err => {
        this.errorMsg = err.message
        this.spinnerService.hide()
      })
    }, () => {})
  }

}
