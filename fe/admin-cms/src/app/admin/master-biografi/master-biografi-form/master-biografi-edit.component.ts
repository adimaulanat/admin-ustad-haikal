import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterBiografiFormComponent } from './master-biografi-form.component';
import { BiografiServices } from 'src/providers/biografi-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-biografi-edit',
  template: `
    <master-biografi-form (init)="init($event)" (save)="save($event)"></master-biografi-form>
  `,
  styles: []
})
export class MasterBiografiEditComponent implements OnInit {

  @ViewChild(MasterBiografiFormComponent)
  formComponent: MasterBiografiFormComponent
  biografi
  progress: { percentage: number } = { percentage: 0 };



  constructor(
    private router: Router, private route: ActivatedRoute, private biografiServices: BiografiServices, private spinnerService: Ng4LoadingSpinnerService
  ) {

  }

  ngOnInit() {
  }

  init(fg: FormGroup) {
    this.spinnerService.show()
    this.route.params.subscribe(params => {
      let id = params['id']
      console.log(id)
      this.biografiServices.getBiografiById(id).subscribe(data => {
        this.biografi = data
        console.log(this.biografi)
        fg.patchValue(this.biografi);
        this.spinnerService.hide()
      })
    })
  }

  save(model) {
    console.log(model)
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined

    if (model.image_name == this.biografi.image_name) {
      this.spinnerService.show()
      this.biografiServices.updateBiografi(this.biografi.id, model).subscribe(() => {
        this.router.navigate(['/admin', 'biografi'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, err => {
        this.formComponent.errorMsg = err.message
        this.formComponent.submitting = false
        this.spinnerService.hide()
      })
    } else {
      this.spinnerService.show()
      this.biografiServices.pushFileToStorage(model, this.progress, true, this.biografi.id)
      setTimeout(() => {
        this.router.navigate(['/admin', 'biografi'])
        this.formComponent.submitting = false
        this.spinnerService.hide()
      }, 6500)
    }

    // this.biografiServices.updateBiografi(this.biografi.id, model).subscribe(data => {
    //   console.log(data)
    //   this.router.navigate(['/admin', 'biografi'])
    //   this.formComponent.submitting = false
    // }, err => {
    //   this.formComponent.errorMsg = err.message
    //   this.formComponent.submitting = false
    // });
  }

}
