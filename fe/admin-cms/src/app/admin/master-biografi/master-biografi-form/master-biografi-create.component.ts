import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterBiografiFormComponent } from './master-biografi-form.component';
import { BiografiServices } from 'src/providers/biografi-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'master-biografi-create',
  template: `
    <master-biografi-form (save)="save($event)"></master-biografi-form>
  `,
  styles: []
})
export class MasterBiografiCreateComponent implements OnInit {

  @ViewChild(MasterBiografiFormComponent)
  formComponent: MasterBiografiFormComponent
  progress: { percentage: number } = { percentage: 0 };
  constructor(
    private router:Router, private biografiServices: BiografiServices, private spinnerService: Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {

  }

  save(model) {
    this.formComponent.submitting = true
    this.formComponent.errorMsg = undefined
    this.spinnerService.show()
    this.biografiServices.pushFileToStorage(model, this.progress);
    setTimeout(() => {
      this.router.navigate(['/admin', 'biografi'])
      this.formComponent.submitting = false
      this.spinnerService.hide()
    },6500)
    
  }

}
