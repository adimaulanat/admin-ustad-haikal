import { Component, OnInit } from '@angular/core';
import { promptDialog } from 'src/shared/promp.modal';
import { BiografiServices } from 'src/providers/biografi-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-master-biografi-browse',
  templateUrl: './master-biografi-browse.component.html',
  styleUrls: ['./master-biografi-browse.component.scss']
})
export class MasterBiografiBrowseComponent implements OnInit {

  errorMsg
  masterBiografi: any = [];

  constructor(private biografiServices: BiografiServices, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show()
    this.biografiServices.getMasterBiografi().subscribe((response) => {
      console.log(response);
      this.masterBiografi = response;
      this.spinnerService.hide()
    },(err) => {
      console.log(err);
      this.spinnerService.hide()
    })
  }

  delete(item) {
    this.errorMsg = undefined

    promptDialog('Delete this record?', 'after deleting, the record will not be recoverable', () => {
      this.spinnerService.show()
      this.biografiServices.deleteBiografi(item.id).subscribe(data => {
        this.masterBiografi = this.masterBiografi.filter(u => u.id !== item.id)
        this.spinnerService.hide()
      }, err => {
        this.errorMsg = err.message
        this.spinnerService.hide()
      })
    }, () => {})
  }

}
