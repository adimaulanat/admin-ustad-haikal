import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { MasterMediaFormComponent } from './master-media-form.component';
import { MediaServices } from 'src/providers/media-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'master-media-create',
    template: `
    <master-media-form (save)="save($event)"></master-media-form>
  `,
    styles: []
})
export class MasterMediaCreateComponent implements OnInit {

    @ViewChild(MasterMediaFormComponent)
    formComponent: MasterMediaFormComponent
    progress: { percentage: number } = { percentage: 0 };

    constructor(
        private router: Router, private mediaServices: MediaServices, private spinnerService: Ng4LoadingSpinnerService
    ) { }

    ngOnInit() {

    }

    save(model) {
        // model.created_date = formatDate(new Date(), 'yyyy/MM/dd', 'en');
        this.formComponent.submitting = true
        this.formComponent.errorMsg = undefined
        if (model.media_type == 'image') {
            this.spinnerService.show()
            this.mediaServices.pushFileToStorage(model, this.progress);
            setTimeout(() => {
                this.router.navigate(['/admin', 'media'])
                this.formComponent.submitting = false
                this.spinnerService.hide()
            }, 6500)
        } else {
            this.spinnerService.show()
            this.mediaServices.createMedia(model).subscribe(() => {
                this.router.navigate(['/admin', 'media'])
                  this.formComponent.submitting = false
                  this.spinnerService.hide()
                }, (err) => {
                  this.formComponent.errorMsg = err.message
                  this.formComponent.submitting = false
                  this.spinnerService.hide()
            })
        }


    }

}
