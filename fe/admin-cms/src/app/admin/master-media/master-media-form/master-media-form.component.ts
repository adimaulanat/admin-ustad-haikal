import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FileUpload } from 'src/shared/models/file-upload';
import { Router } from '@angular/router';
import { FileValidator } from 'src/shared/validators/input-file.validators';

@Component({
  selector: 'master-media-form',
  templateUrl: './master-media-form.component.html',
  styleUrls: ['./master-media-form.component.scss']
})
export class MasterMediaFormComponent implements OnInit {

  @Output('init') onInit = new EventEmitter()
  @Output('save') onSave = new EventEmitter()

  fg:FormGroup
  errorMsg
  submitting
  currentFileUpload: FileUpload

  constructor(
    private fb:FormBuilder,
    private router:Router
  ) { 
    this.fg = this.fb.group({
      title: [null, [Validators.required]],
      description: [null, [Validators.required]],
      media_type: [null, [Validators.required]],
      file_url: new FormControl("", [FileValidator.validate])
    })
  }

  ngOnInit() {
    this.onInit.emit(this.fg)
  }

  save() {
    this.onSave.emit(this.fg.value)
  }

  cancel() {
    this.router.navigate(['/admin', 'media'])
  }

  onFileChanged(event){

    if(event.target.files && event.target.files.length) {
      this.currentFileUpload = new FileUpload(<File>event.target.files[0]);
        this.fg.patchValue({
          file_url: this.currentFileUpload
       });
    }
  }

  onCheck(){
    console.log(this.fg.value.media_type)
  }

}
