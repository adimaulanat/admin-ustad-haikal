import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MasterMediaFormComponent } from './master-media-form.component';
import { MediaServices } from 'src/providers/media-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'master-media-edit',
    template: `
    <master-media-form (init)="init($event)" (save)="save($event)"></master-media-form>
  `,
    styles: []
})
export class MasterMediaEditComponent implements OnInit {

    @ViewChild(MasterMediaFormComponent)
    formComponent: MasterMediaFormComponent
    media
    progress: { percentage: number } = { percentage: 0 };



    constructor(
        private router: Router, private route: ActivatedRoute, private mediaServices: MediaServices, private spinnerService: Ng4LoadingSpinnerService
    ) {

    }

    ngOnInit() {
    }

    init(fg: FormGroup) {
        this.spinnerService.show()
        this.route.params.subscribe(params => {
            let id = params['id']
            console.log(id)
            this.mediaServices.getMediaById(id).subscribe(data => {
                this.media = data
                console.log(this.media)
                fg.patchValue(this.media);
                this.spinnerService.hide()
            })
        })
    }

    save(model) {
        this.formComponent.submitting = true
        this.formComponent.errorMsg = undefined
        if (model.media_type == 'image') {
            if (model.file_url === this.media.file_url) {
                this.updateMedia(model);
            } else {
                this.spinnerService.show()
                this.mediaServices.pushFileToStorage(model, this.progress, true, this.media.id)
                setTimeout(() => {
                    this.router.navigate(['/admin', 'media'])
                    this.formComponent.submitting = false
                    this.spinnerService.hide()
                }, 6500)
            }
        } else {
            this.updateMedia(model);
        }

    }

    updateMedia(model) {
        this.spinnerService.show()
        this.mediaServices.updateMedia(this.media.id, model).subscribe(() => {
            this.router.navigate(['/admin', 'media'])
            this.formComponent.submitting = false
            this.spinnerService.hide()
        }, err => {
            this.formComponent.errorMsg = err.message
            this.formComponent.submitting = false
            this.spinnerService.hide()
        })
    }

}
