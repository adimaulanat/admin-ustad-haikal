import { Component, OnInit } from '@angular/core';
import { promptDialog } from 'src/shared/promp.modal';
import { MediaServices } from 'src/providers/media-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-master-media-browse',
  templateUrl: './master-media-browse.component.html',
  styleUrls: ['./master-media-browse.component.scss']
})
export class MasterMediaBrowseComponent implements OnInit {

  errorMsg
  masterMedia: any = [];

  constructor(private mediaServices: MediaServices, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show()
    this.mediaServices.getMasterMedia().subscribe((response) => {
      console.log(response);
      this.masterMedia = response;
      this.spinnerService.hide()
    },(err) => {
      console.log(err);
      this.spinnerService.hide()
    })
  }

  delete(item) {
    this.errorMsg = undefined

    promptDialog('Delete this record?', 'after deleting, the record will not be recoverable', () => {
      this.spinnerService.show()
      this.mediaServices.deleteMedia(item.id).subscribe(data => {
        this.masterMedia = this.masterMedia.filter(u => u.id !== item.id)
        this.spinnerService.hide()
      }, err => {
        this.errorMsg = err.message
        this.spinnerService.hide()
      })
    }, () => {})
  }
}
