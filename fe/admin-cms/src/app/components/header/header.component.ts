import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServices } from 'src/providers/auth-services';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedin = localStorage.getItem('currentUser');
  isLoggedIn$: Observable<boolean>;
  statusLoggedIn: boolean


  constructor(private authServices: AuthServices, private router: Router) { 
    authServices.getLoggedInStatus.subscribe(res => this.statusLoggedIn = res)
    console.log(this.statusLoggedIn)
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authServices.isLoggedIn;
    console.log(this.isLoggedIn$)
    this.loggedin = localStorage.getItem('currentUser');
  }

  logout() {
    // remove user from local storage to log user out
    this.authServices.logout();
    this.router.navigate(['/login']);
  }

}
