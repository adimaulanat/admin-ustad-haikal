import { Component } from '@angular/core';
import { AuthServices } from 'src/providers/auth-services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'admin-cms';
  loggedin = 'first'

  constructor(private authServices: AuthServices, private router: Router) {
    console.log(this.loggedin)
  }

  logout() {
    // remove user from local storage to log user out
    this.authServices.logout();
    this.router.navigate(['/login']);
  }
}
