import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthServices } from 'src/providers/auth-services';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.component.html',
  styleUrls: ['./form-register.component.scss']
})
export class FormRegisterComponent implements OnInit {
  registrationForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private authServices: AuthServices, private router: Router, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      c_password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registrationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registrationForm.valid) {
      if (this.registrationForm.value.password == this.registrationForm.value.c_password) {
        this.spinnerService.show()
        this.authServices.register(this.registrationForm.value).subscribe((response) => {
          alert("Register Successfull")
          this.router.navigate(['/login']);
          this.spinnerService.hide()
        }, (err) => {
          console.log(err);
          alert("Regitstrasi Gagal");
          this.spinnerService.hide()
        })
      } else {
        alert("The new password don't match")
      }
    } else {
      return;
    }

  }

}
