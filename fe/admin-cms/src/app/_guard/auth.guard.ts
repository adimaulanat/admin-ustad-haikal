import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthServices } from 'src/providers/auth-services';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthServices) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.authService.isLoggedIn.pipe(
        take(1),
        map((isLoggedIn : boolean) => {
          if(!localStorage.getItem('currentUser')){
            this.router.navigate(['/login']);
            return false
          } else {
            this.authService.makeLoginTrue();
            return true
          }
        })
      )
      
      // console.log(localStorage.getItem('currentUser'))
      // if(localStorage.getItem('currentUser')){
      //   return true;
      // }else{
      //   this.router.navigate(['/login']);
      //   return false;
      // }
  }
}
