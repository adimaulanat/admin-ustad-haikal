import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServices } from 'src/providers/auth-services';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})

export class FormLoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor( private formBuilder: FormBuilder, private authServices : AuthServices, private router: Router, private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.valid) {
      this.spinnerService.show()
      let body = {
        'email' : this.loginForm.value.email,
        'password' : this.loginForm.value.password
      }
      this.authServices.login(body).subscribe((response) => {
        this.authServices.makeLoginTrue();
        if(response.success.id && response.success.token){
          // this.authServices.
          localStorage.setItem('currentUser', body.email);
          this.router.navigate(['/admin']);
          this.spinnerService.hide()
        }
      },(err) => {
        console.log(err);
        alert("Login Gagal");
        // this.authServices.makeLoginTrue();
        // this.router.navigate(['/admin']);
        // localStorage.setItem('currentUser', 'testing');
        this.spinnerService.hide()
      })
    }else{
      return;
    }
    
}
}
